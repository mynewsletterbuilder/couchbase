.PHONY: docker

IMAGE_BASE = jbanetwork/
IMAGE = couchbase
MY_PWD = $(shell pwd)

all: php

php:
	docker build -t $(IMAGE_BASE)$(IMAGE) -f $(MY_PWD)/Dockerfile $(MY_PWD)
ifdef PUSH
	docker push $(IMAGE_BASE)$(IMAGE):$(TAG)
endif