#!/usr/bin/env bash

chown -R couchbase:couchbase /opt/couchbase/var

if [ ! -f /opt/couchbase/var/.SETUP ]; then
	# this makes sure that persistent storage
	# starts out with the appropriate directory
	# structure
	rsync -av /opt/couchbase/var_base/ /opt/couchbase/var/
	/etc/init.d/couchbase-server start

	UP=0;
	while [ ${UP} == 0 ]; do
		curl -s http://127.0.0.1:8091/pools;
		if [ $? == 0 ];then
			UP=1;
		else
			printf "waiting for couchbase...\r"
			sleep 2;
		fi
	done

	if [ -z "COUCHBASE_INDEX_MEM" ]; then
		COUCHBASE_INDEX_MEM=$COUCHBASE_MEM
	fi

	curl -v -X POST http://127.0.0.1:8091/pools/default -d memoryQuota=${COUCHBASE_MEM} -d indexMemoryQuota=${COUCHBASE_INDEX_MEM};
	if [ "$TYPE" = "master" ]; then
		curl -v http://127.0.0.1:8091/pools/default/buckets -d bucketType=couchbase -d name=mm -d authType=sasl -d ramQuotaMB=${COUCHBASE_MEM} -d replicaNumber=1 -d threadsNumber=8;
		sleep 2;
		curl -X PUT \
			-H 'Content-Type: application/json' \
			http://127.0.0.1:8092/mm/_design/logs \
			-d @logs.ddoc

		curl -X PUT \
			-H 'Content-Type: application/json' \
			http://127.0.0.1:8092/mm/_design/addresses \
			-d @addresses.ddoc

		curl -X PUT \
			-H 'Content-Type: application/json' \
			http://127.0.0.1:8092/mm/_design/newsletters \
			-d @newsletters.ddoc
	fi

	if [ -z "$COUCHBASE_SERVICES" ]; then
		COUCHBASE_SERVICES="kv%2Cn1ql%2Cindex"
	fi
	curl -v http://127.0.0.1:8091/node/controller/setupServices -d services=${COUCHBASE_SERVICES};

	if [ -z "$COUCHBASE_USER" ]; then
		COUCHBASE_USER="Administrator"
	fi
	curl -v http://127.0.0.1:8091/settings/web -d port=8091 -d username=${COUCHBASE_USER} -d password=${COUCHBASE_PASSWORD};

	COUCHBASE_MASTER_IP=$(getent hosts ${COUCHBASE_MASTER} | awk '{ print $1 }');

	echo "Type: ${TYPE}, Master: ${COUCHBASE_MASTER}, MasterIp: ${COUCHBASE_MASTER_IP}";

	if [ "$TYPE" = "worker" ]; then
		sleep 5;
		UP=0;
		while [ ${UP} == 0 ]; do
			curl -s http://${COUCHBASE_MASTER_IP}:8091/pools;
			if [ $? == 0 ];then
				UP=1;
			else
				printf "waiting for couchbase...\r"
				sleep 2;
			fi
		done
		IP=$(hostname -I | awk '{ print $1 }');
		#couchbase-cli server-add -c ${COUCHBASE_MASTER_IP}:8091 -u Administrator -p ${COUCHBASE_PASSWORD} --server-add=${IP} --services=data,index,query --server-add-username=Administrator --server-add-password=${COUCHBASE_PASSWORD};
		#sleep 10;
		couchbase-cli rebalance -c ${COUCHBASE_MASTER_IP}:8091 -u Administrator -p ${COUCHBASE_PASSWORD} --server-add=${IP} --services=data,index,query --server-add-username=Administrator --server-add-password=${COUCHBASE_PASSWORD};
	fi;

    touch /opt/couchbase/var/.SETUP
    /etc/init.d/couchbase-server stop
fi;

[[ "$1" == "couchbase-server" ]] && {
    echo "Starting Couchbase Server -- Web UI available at http://<ip>:8091 and logs available in /opt/couchbase/var/lib/couchbase/logs"
    exec /usr/sbin/runsvdir-start
}

exec "$@"
