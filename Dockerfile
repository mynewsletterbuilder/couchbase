FROM couchbase:enterprise-4.6.1

# we move the var dir out of the way so that we can
# mount it for persistent storage and rsync it later
RUN apt-get update && \
    apt-get install -yq rsync && \
    rsync -av /opt/couchbase/var/ /opt/couchbase/var_base/ && \
    apt-get autoremove && apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY docker-entrypoint.sh /opt/couchbase/docker-entrypoint.sh
COPY addresses.ddoc /opt/couchbase/addresses.ddoc
COPY newsletters.ddoc /opt/couchbase/newsletters.ddoc
COPY logs.ddoc /opt/couchbase/logs.ddoc

WORKDIR /opt/couchbase

ENTRYPOINT ["/opt/couchbase/docker-entrypoint.sh"]
CMD ["couchbase-server"]
